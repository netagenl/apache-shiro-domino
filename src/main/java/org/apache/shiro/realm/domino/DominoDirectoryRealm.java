package org.apache.shiro.realm.domino;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.activedirectory.ActiveDirectoryRealm;
import org.apache.shiro.realm.ldap.LdapContextFactory;
import org.apache.shiro.realm.ldap.LdapUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DominoDirectoryRealm extends ActiveDirectoryRealm {

	private static final Logger log = LoggerFactory.getLogger(DominoDirectoryRealm.class);

	/**
	 * Builds an {@link org.apache.shiro.authz.AuthorizationInfo} object by
	 * querying the active directory LDAP context for the groups that a user is
	 * a member of. The groups are then translated to role names by using the
	 * configured {@link #groupRolesMap}.
	 * <p/>
	 * This implementation expects the <tt>principal</tt> argument to be a
	 * String username.
	 * <p/>
	 * Subclasses can override this method to determine authorization data
	 * (roles, permissions, etc) in a more complex way. Note that this default
	 * implementation does not support permissions, only roles.
	 *
	 * @param principals
	 *            the principal of the Subject whose account is being retrieved.
	 * @param ldapContextFactory
	 *            the factory used to create LDAP connections.
	 * @return the AuthorizationInfo for the given Subject principal.
	 * @throws NamingException
	 *             if an error occurs when searching the LDAP server.
	 */
	@Override
	protected AuthorizationInfo queryForAuthorizationInfo(PrincipalCollection principals,
			LdapContextFactory ldapContextFactory) throws NamingException {

		String username = (String) getAvailablePrincipal(principals);
		// Perform context search
		LdapContext ldapContext = ldapContextFactory.getSystemLdapContext();

		Set<String> roleNames;

		try {
			roleNames = getRoleNamesForUser(username, ldapContext);
		} finally {
			LdapUtils.closeContext(ldapContext);
		}

		return buildAuthorizationInfo(roleNames);
	}

	protected Set<String> getRoleNamesForUser(String username, LdapContext ldapContext) throws NamingException {
		Set<String> roleNames;
		roleNames = new LinkedHashSet<String>();

		SearchControls searchCtls = new SearchControls();
		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		String userPrincipalName = username;
		if (principalSuffix != null) {
			userPrincipalName += principalSuffix;
		}
	
		String getDNFromUser = "(&(objectClass=person)(CN={0}))";

		// SHIRO-115 - prevent potential code injection:
		String searchGroup = "(&(objectClass=*)(member={0}))";
		Object[] searchArguments = new Object[] { userPrincipalName };
	
		NamingEnumeration<SearchResult> answer = ldapContext.search(searchBase, getDNFromUser, searchArguments, searchCtls);

		while (answer.hasMoreElements()) {
			SearchResult sr = (SearchResult) answer.next();

			Object[] searchDN = new Object[] { sr.getNameInNamespace() };
			if (log.isDebugEnabled()) {
				log.debug("Retrieving group names for user [" + sr.getNameInNamespace() + "]");
			}

			NamingEnumeration<SearchResult> groups = ldapContext.search(searchBase, searchGroup, searchDN, searchCtls);

			Collection<String> groupNames = new ArrayList<String>() {
				/**
				* 
				*/
				private static final long serialVersionUID = 1L;

				{

				}
			};

			while (groups.hasMoreElements()) {
				SearchResult groupResult = (SearchResult) groups.next();
				
				groupNames.add(groupResult.getName());
				 if (log.isDebugEnabled()) {
	                 log.debug("Group found for user [" + userPrincipalName + "]: " + groupResult.getName());
	             }
			
			}
			
			Collection<String> rolesForGroups = getRoleNamesForGroups(groupNames);
			roleNames.addAll(rolesForGroups);

		}
		return roleNames;
	}
}
